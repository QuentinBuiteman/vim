# vim
This is my personal vim setup which I use on Ubuntu.

## Installation
```
cd ~
git clone https://bitbucket.com/QuentinBuiteman/vim.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
cd ~/.vim
git submodule init
git submodule update
```
