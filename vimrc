" don't make vim compatible with vi
set nocompatible

" Set clipboard as standard copy location
set clipboard=unnamed
set clipboard-=autoselect

" Set undodir
set undofile
set undodir=~/.vim/undodir

" Activatie Pathogen
execute pathogen#infect()

" Turn off file specific behaviour
filetype plugin indent off

" Set unix fileformat
set fileformat=unix
set fileformats=unix

" Set utf-8 encoding
set encoding=utf-8
set fileencoding=utf-8

" Reload files changed outside vim
set autoread

" Don't redraw screen while running macros, etc
set lazyredraw

" Set backspace behaviour
set backspace=indent,eol,start

" Color scheme selection
set background=dark
colorscheme hybrid_material
let g:enable_bold_font = 1
let g:enable_italic_font = 1
let g:airline_theme='hybrid'
let g:airline_powerline_fonts = 1

" Turn on syntax
syntax on
let g:jsx_ext_required = 0

" Don't wrap lines
set nowrap

" Line numbers
set number
set nuw=4
hi NonText guifg=bg

" Highlight current line
set cursorline

" Last line in PHP
autocmd Filetype php setlocal noeol

" Tab settings
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smartindent
autocmd Filetype php setlocal ts=4 sw=4
autocmd Filetype html.twig setlocal ts=4 sw=4
autocmd Filetype vue setlocal ts=4 sw=4

" Navigate windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
let g:syntastic_html_checkers = []
let g:syntastic_scss_checkers = []

function CheckLinter(type, filepath, linter, args)
  if exists('b:syntastic_checkers')
    return
  endif

  if filereadable(a:filepath)
    let b:syntastic_checkers = [a:linter]
    let {'b:syntastic_' . a:linter . '_exec'} = a:filepath
    let {'b:syntastic_' . a:type . '_' . a:linter . '_args'} = a:args
  endif
endfunction

function SetupPHPLinter()
  let l:current_folder = expand('%:p:h')
  let l:project_folder = fnamemodify(syntastic#util#findFileInParent('package.json', l:current_folder), ':h')
  let l:bin_folder = l:project_folder . '/vendor/bin/'
  call CheckLinter('php', l:bin_folder . 'phpcs', 'phpcs', '--standard=' . l:project_folder . '/phpcs.xml')
endfunction

autocmd FileType php call SetupPHPLinter()

" Graphical vim options
if has('gui_running')
  " NERDTree settings
  autocmd vimenter * NERDTree

  " Turn off toolbars
  set guioptions-=m
  set guioptions-=T
  set guioptions-=r
  set guioptions-=L

  " Turn off auto copy
  set guioptions-=a

  " Turn off borders
  set guiheadroom=0

  " Set GUI Font
  if has("gui_macvim")
    set guifont=Inconsolata-dz\ for\ Powerline:h14
  else
    set guifont=Inconsolata-dz\ for\ Powerline\ 13
  endif

  set anti enc=utf-8
  set linespace=10
end

" 80 Line width for git commit messages
autocmd FileType gitcommit set textwidth=72
set colorcolumn=+1
autocmd FileType gitcommit set colorcolumn+=51
